# -*- coding: utf-8 -*-
#   This file is part of PyMSES.
#
#   PyMSES is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyMSES is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyMSES.  If not, see <http://www.gnu.org/licenses/>.
r"""
:mod:`pymses.analysis.datamap` --- Data map module
--------------------------------------------------

"""
from PIL import Image as I
from matplotlib import pyplot as P
from matplotlib.colors import Colormap, LinearSegmentedColormap
from matplotlib.figure import Figure
from matplotlib.ticker import IndexLocator, FormatStrFormatter
import numpy as N
import os

from .camera import Camera
from pymses.utils import HDF5Serializable
from pymses.utils.colormaps import Colormaps
from pymses.utils.constants import Unit
from pymses.utils.filename import FileUtil


class DataMapFigure(Figure):
    def display_vector_field(self, vecx, vecy):
        pass


class DataMap(HDF5Serializable):
    """
    Data map class

    Parameters
    ----------
    map: ``numpy.ndarray``
        raw data
    camera: :class:`~pymses.analysis.camera.Camera`
        camera used to obtain the data
    unit: :class:`~pymses.utils.constants.Unit`

    name: ``string``
        name of the map (optional), default None.
    """

    __version = 1

    def __init__(self, vmap, camera, unit, name=None, mask=None):
        super(DataMap, self).__init__()

        self._vmap = N.asarray(vmap)

        if not isinstance(self._vmap, N.ndarray) or self._vmap.ndim != 2:
            raise AttributeError("vmap is not a valid 2D numpy.ndarray instance.")

        if mask is None:
            self._map_mask = camera.get_map_mask()
        else:
            if self._vmap.shape != mask.shape:
                raise AttributeError("mask array shape is not identical to the map array shape !")
            if mask.min() < 0.0 or mask.max() > 1.0:
                raise AttributeError("mask array values must lie within [0.0; 1.0].")
            self._map_mask = mask

        if not isinstance(camera, Camera):
            raise AttributeError("'camera' attribute is not a valid Camera instance")
        self._camera = camera.copy()

        # Set default is_log value (based on Camera.log_sensitive)
        self._set_log_sensitive()

        # Get image size + init R, G, B, Alpha bands to None
        self._nx, self._ny = camera.get_map_size()
        self._R_band = None
        self._G_band = None
        self._B_band = None
        self._A_band = None

        if self._nx != self._vmap.shape[0] or self._ny != self._vmap.shape[1]:
            raise AttributeError("Camera size doesn't  match the map dimensions")

        if name is not None and not isinstance(name, basestring):
            raise AttributeError("'name' attribute must be a string")
        self.name = name

        # Mandatory map value unit
        if not isinstance(unit, Unit):
            raise AttributeError("'unit' is not a valid Unit instance")
        self._unit = unit

    def __eq__(self, other):
        if not N.allclose(self._vmap, other._vmap, rtol=1.0e-6):
            return False

        if not self._camera != other.camera:
            return False

        if self.name != other.name:
            return False
        return True

    @property
    def map(self):
        """
        Get the data map array

        Returns
        -------
        map: numpy.ndarray
            data map
        """
        return self._vmap

    @property
    def camera(self):
        """
        Get the data map camera object

        Returns
        -------
        cam: :class:~pymses.analysis.camera.Camera`
            data map camera instance
        """
        return self._camera

    @property
    def map_unit(self):
        """
        Get the data map value unit

        Returns
        -------
        cam: :class:~pymses.utils.constants.unit.Unit`
            data map value unit
        """
        return self._unit

    def value_range(self, vrange=None, fraction=1.0):
        """
        Get the map value range.

            * if a user-defined vrange is given, then it is used to compute the map range values,
            * if not, the map range values is computed from a fraction (percent) of the total value
              of the map parameter. the min. map range value is defined as the value below which there
              is a fraction of the map (default 1 %)

        Parameters
        ----------
        vrange     : user-defined map value range.
        fraction   : fraction of the total map values below the min. map range (in percent). Default 1 %.

        Returns
        -------
        map_range : (``float``, ``float``) ``tuple``
            the map range values (vmin, vmax)
        """
        # Get a copy of the transposed value map
        m = self._vmap.copy().transpose()
        return self._vmap_range(m, vrange, fraction, verbose=False)

    def _set_log_sensitive(self, log_sensitive=None):
        """
        Define the logarithmic scale sensitivity of the DataMap

        Parameters
        ----------
        log_sensitive: ``bool`` or None
            Does the datamap need to be displayed in logarithmic scale ? Default None, the default value is then taken
            from the Camera.log_sensitive attribute.
        """
        if log_sensitive is None:
            self._is_log = self._camera.log_sensitive
        else:
            if not isinstance(log_sensitive, bool):
                raise AttributeError("'log_sensitive' attribute must be a boolean value, if not None.")
            self._is_log = log_sensitive

    def _vmap_range(self, mt, vrange=None, fraction=1.0, verbose=False):
        """
        Map range computation function. Computes the linear/log scale map value range

            * if a user-defined vrange is given, then it is used to compute the map range values,
            * if not, the map range values is computed from a fraction (percent) of the total value
              of the map parameter. the min. map range value is defined as the value below which there
              is a fraction of the map (default 1 %)

        Parameters
        ----------
        mt         : 2D transposed map from which the map range values are computed
        vrange     : user-defined map value range.
        fraction   : fraction of the total map values below the min. map range (in percent). Default 1 %.

        Returns
        -------
        map_range : (``float``, ``float``) ``tuple``
            the map range values (vmin, vmax)
        """
        range_mvalues = mt[self._map_mask.transpose() > 0.0].ravel()

        if range_mvalues.size == 0:  # Map mask is completely null => region outside of simulation domain
            return None, None

        vmin = None
        vmax = None

        if vrange is not None:  # Unfold vrange into a (vmin, vmax) tuple
            if not isinstance(vrange, tuple):
                raise AttributeError("vrange parameter must be a (vmin, vmax) tuple. Got %s" % type(vrange))
            if vrange[1] is not None:
                vmax = vrange[1]

            if vrange[0] is not None:
                vmin = vrange[0]

        if vmin is None or vmax is None:
            if vmax is None:  # Default vmax = map maximum value
                vmax = range_mvalues.max()

            # Fallback values to (None, None) when all map values are negative or null in log scale view
            if self._is_log and vmax <= 0.0:
                raise ValueError("Cannot compute map range when all log-scaled map values are negative.")

            if vmin is None:
                if fraction < 0.0 or fraction > 100.0:
                    raise AttributeError("fraction parameter must be in the range [0.0, 100.0]. Got %s" % fraction)
                frac = fraction / 100.0

                if frac == 0.0:
                    if self._is_log:
                        vmin = range_mvalues[range_mvalues > 0.0].min()
                    else:
                        vmin = range_mvalues.min()
                elif frac == 1.0:
                    vmin = vmax
                else:
                    if self._is_log:  # Sort strictly positive values of the map value list
                        values = N.sort(range_mvalues[range_mvalues > 0.0])
                        weights = N.log10(values)
                    else:  # Sort values of the masked map
                        values = N.sort(range_mvalues)
                        weights = values.copy()

                    wrange = weights[-1] - weights[0]

                    # Flat map case
                    if wrange == 0.0:
                        vmin = values[0]
                    else:
                        weights = (weights - weights[0]) / wrange
                        # vmin = N.percentile((weights-weights[0])/wrange, q=frac*100.0, interpolation='higher')
                        cumval = N.cumsum(weights)
                        cumval /= cumval[-1]
                        mask = (cumval >= frac)
                        vmin = values[mask][0]

        if verbose:
            print "Map value range is : [%g %g]" % (vmin, vmax)
        return vmin, vmax

    def _colormap(self, cmap, vrange_out, discrete, verbose=False):
        """
        Compute colormap
        :param cmap:
        :param vrange_out:
        :param discrete:
        :param verbose:
        :return:
        """
        vmin, vmax = vrange_out

        if self._is_log:
            if vmin is not None:
                vmin = N.log10(vmin)
            if vmax is not None:
                vmax = N.log10(vmax)

        if discrete:
            if vmin is None and vmax is None:
                vmin = 0.0
                vmax = 1.0
            colormap, vrange = Colormaps.get_cmap(cmap, discrete=True, value_range=(vmin, vmax))
            vmin, vmax = vrange
        else:
            colormap = Colormaps.get_cmap(cmap)

        vrange_cmap = (vmin, vmax)

        if verbose:
            print "Colormap range is : [%g %g]" % (vmin, vmax)

        return colormap, vrange_cmap

    @classmethod
    def _apply_log_scale(cls, vmap, vrange, verbose=False):
        """

        Parameters
        ----------
        vmap: (nxx, ny) ``numpy.ndarray``
            map values 2D array
        verbose: ``bool``
            is verbose ? default False.
        """
        # Apply log-scale map
        neg_null_mask = vmap <= 0.0

        if neg_null_mask.all():  # No positive values ?
            raise ValueError("Cannot apply log scale : all map values are <= 0 !")

        if neg_null_mask.any():  # Clip negative or null map values to apply log scale safely
            if vrange[0] is not None:
                non_null_min_map = 10.0 ** vrange[0]  # value range minimum log value
            else:  # Should never happen -> all map values must be null if mask is null
                non_null_min_map = N.min(vmap[~neg_null_mask]) / 10.0  # 1/10th of the lowest positive map value

            # Clipping
            vmap[neg_null_mask] = non_null_min_map
            if verbose:
                print "Warning: %d values <= 0 were replaced by min_map/10 = %f in order to apply log scale" % \
                      (N.sum(neg_null_mask), non_null_min_map)

        # Inplace log-scaling
        N.log10(vmap, out=vmap)

    @classmethod
    def _clip_map(cls, vmap, vrange):
        # Clip map values
        if vrange[0] is not None:
            vmap[vmap < vrange[0]] = vrange[0]
        if vrange[1] is not None:
            vmap[vmap > vrange[1]] = vrange[1]

    def save_PNG(self, img_fname=None, vrange=None, fraction=1.0, cmap="Viridis", discrete=False, is_log_values=None,
                 alpha_mask=True, verbose=False):
        """
        Convert the map into a PIL Image and save it into a PNG file.

        Parameters
        ----------
        img_fname: ``string``
            PNG image file name or path
        vrange: ``tuple`` or `None`
            linear-scale map value range (vmin, vmax) tuple used for clipping before saving the PNG file. Default is
            None. When left to None, the map value range is computed automatically. One boundary may be set by
            defining (vmin, None) or (None, vmax).
        fraction: ``float``
            Used by automatic map value range computation. Fraction of the cumulated map values (linear or log scaled)
            below the min. map range (in percent). Default : 1 %.
        cmap: ``string`` or ``matplotlib.colors.Colormap``
            colormap applied to the map to generate the PIL Image.
        discrete: ``bool``
            Show only discrete values in colormap. Default : False.
        is_log_values: ``bool`` or `None`
            True if the image file is a log-scale view of the map, otherwise False. default None. If None, the
            `log_sensitive` parameter of the Datamap : class:`~pymses.analysis.camera.Camera` instance is used.
        alpha_mask: ``bool``
            Use the map mask to generate the PNG file alpha band ? Default : True.
        verbose: ``bool``
            Verbose processing ? Default : False.
        """
        # Get a copy of the value map
        m = self._vmap.copy().transpose()

        # Log-scaled map values ? Default => camera.log_sensitive
        self._set_log_sensitive(is_log_values)

        vrange_out = self._vmap_range(m, vrange, fraction, verbose)

        colormap, vrange_cmap = self._colormap(cmap, vrange_out, discrete, verbose)

        # Log scale ?
        if self._is_log:
            self._apply_log_scale(m, vrange_cmap, verbose)

        # Clipping
        self._clip_map(m, vrange_cmap)

        if vrange_cmap[0] is not None:
            m -= vrange_cmap[0]
            if vrange_cmap[1] is not None and vrange_cmap[1] > vrange_cmap[0]:
                m /= vrange_cmap[1] - vrange_cmap[0]

        m = N.clip(N.round(colormap(m) * 256.0).astype('i'), 0, 255).reshape(self._nx * self._ny, 4)
        # if ramses_output is not None:
        # if drawStarsParam is None:
        #     drawStarsParam = DrawStarsParameters()
        #     draw_stars(map, ramses_output, cam, drawStarsParam)

        if self._R_band is None:
            self._R_band = I.new("L", (self._nx, self._ny))
        self._R_band.putdata(m[:, 0])
        if self._G_band is None:
            self._G_band = I.new("L", (self._nx, self._ny))
        self._G_band.putdata(m[:, 1])
        if self._B_band is None:
            self._B_band = I.new("L", (self._nx, self._ny))
        self._B_band.putdata(m[:, 2])
        if alpha_mask:
            map_mask = N.clip(N.round(256.0 * self._map_mask).astype('i'), 0, 255).transpose()
            map_mask = map_mask.reshape(self._nx * self._ny)
            if self._A_band is None:
                self._A_band = I.new("L", (self._nx, self._ny))
            self._A_band.putdata(map_mask)
            out_img = I.merge("RGBA", (self._R_band, self._G_band, self._B_band, self._A_band))
        else:
            out_img = I.merge("RGB", (self._R_band, self._G_band, self._B_band))

        # Flip top <-> bottom (image origin corner is top left corner)
        out_img = out_img.transpose(I.FLIP_TOP_BOTTOM)

        if img_fname is not None:
            # Save image
            path = FileUtil.new_filepath(img_fname, append_extension=FileUtil.PNG_FILE)
            print "Saving img into '%s'" % path
            out_img.save(path)

        return out_img

    def save_plot(self, plot_fname=None, vrange=None, fraction=1.0, cmap="Viridis", discrete=False, is_log_values=None,
                  axis_unit=None, map_unit=None, verbose=False):
        """
        Convert the map into a matplotlib plot and save it into a PNG file, if required.

        Parameters
        ----------
        plot_fname: ``string`` or None
            PNG image file name or path. Default None : do not save the plot into a PNG file.
        vrange: ``tuple`` or `None`
            linear-scale map value range (vmin, vmax) tuple used for clipping before saving the PNG file. Default is
            None. When left to None, the map value range is computed automatically. One boundary may be set by
            defining (vmin, None) or (None, vmax).The range values are expressed in 'map_unit', if defined. If
            'map_unit' is not defined, the range value is expressed in base map value unit.
        fraction: ``float``
            Used by automatic map value range computation. Fraction of the cumulated map values (linear or log scaled)
            below the min. map range (in percent). Default : 1 %.
        cmap: ``string`` or ``matplotlib.colors.Colormap``
            colormap applied to the map to generate the PIL Image.
        discrete: ``bool``
            Show only discrete values in colormap. Default : False.
        is_log_values: ``bool`` or `None`
            True if the image file is a log-scale view of the map, otherwise False. default None. If None, the
            `log_sensitive` parameter of the Datamap's :class:`~pymses.analysis.camera.Camera` instance is used.
        axis_unit: :class:`~pymses.utils.constants.unit.Unit`
            map axis size unit used in output plot.
        map_unit: :class:`~pymses.utils.constants.unit.Unit`
            map value unit used in output plot.
        verbose: ``bool``
            Verbose processing ? Default : False.
        """
        # Get a copy of the value map
        m = self._vmap.copy().transpose()

        # Log-scaled map values ? Default => camera.log_sensitive
        self._set_log_sensitive(is_log_values)

        # Get u/v axes labels and pixel edge coordinates
        uinfo, vinfo = self._camera.get_uvaxes_edges_labels(axis_unit=axis_unit)
        u_axisname, u_axisunit, u_label_latex, uedges = uinfo
        v_axisname, v_axisunit, v_label_latex, vedges = vinfo

        # Map value unit conversion
        if map_unit is not None and isinstance(map_unit, Unit):
            map_value_factor = self._unit.express(map_unit)
            m *= map_value_factor
            map_unit_label = map_unit.latex
        else:
            map_unit_label = self._unit.latex

        # Set map value range
        vrange_out = self._vmap_range(m, vrange, fraction, verbose)

        colormap, vrange_cmap = self._colormap(cmap, vrange_out, discrete, verbose)

        # Log scale ?
        if self._is_log:
            self._apply_log_scale(m, vrange_cmap, verbose)

        # Clipping
        self._clip_map(m, vrange_cmap)

        # Plot map using matplotlib.pyplot.imshow()
        P.figure(FigureClass=DataMapFigure)
        P.imshow(m, cmap=colormap, vmin=vrange_cmap[0], vmax=vrange_cmap[1], origin='lower',
                 extent=[uedges[0], uedges[-1], vedges[0], vedges[-1]], interpolation='none')

        # Set axis labels
        P.xlabel(u_label_latex)
        P.ylabel(v_label_latex)

        # Pretty user-defined colorbar
        if self._is_log:
            fo = FormatStrFormatter("$10^{%d}$")
            offset = N.ceil(vrange_cmap[0]) - vrange_cmap[0]
            lo = IndexLocator(1.0, offset)
            cb = P.colorbar(ticks=lo, format=fo)
        else:
            if discrete:
                fo = FormatStrFormatter("%d")
                ncol = int(N.round(vrange_cmap[1] - vrange_cmap[0]))
                ti = N.linspace(vrange_cmap[0] + 0.5, vrange_cmap[1] - 0.5, ncol)
                cb = P.colorbar(format=fo, ticks=ti)
            else:
                cb = P.colorbar()

        # Set colorbar label
        cb.set_label(map_unit_label)

        fig = P.gcf()
        # Automatically adjust layout to fit the figure canvas
        fig.tight_layout()

        # Save image
        if plot_fname is not None:
            fname = FileUtil.new_filepath(plot_fname, append_extension=FileUtil.PNG_FILE)
            print "Saving plot into '%s'" % fname
            fig.savefig(fname)

        return fig

    def save_FITS(self, fits_fname, axis_unit=None, map_unit=None):
        r"""
        Function that saves the DataMap into a FITS file.

        Parameters
        ----------
        fits_fname : the FITS file path in which the DataMap is to be saved
        axis_unit: :class:`~pymses.utils.constants.unit.Unit`
            U/V-axis output unit
        map_unit: :class:`~pymses.utils.constants.unit.Unit`
            map value output unit
        """
        try:
            from astropy.io import fits
            from astropy import wcs
        except ImportError:
            raise ImportError("astropy package is not available. It is mandatory to save FITS files.")

        hdr = self._camera.get_fits_header(axis_unit=axis_unit)

        map = self._vmap.copy().transpose()
        if map_unit is not None:
            hdr["BUNIT"] = map_unit.name
            map *= self._unit.express(map_unit)
        else:
            hdr["BUNIT"] = self._unit.name

        fits_dir = os.path.dirname(fits_fname)
        print "Saving image into FITS file '%s'" % fits_fname
        if not os.path.isdir(fits_dir):
            os.makedirs(fits_dir)
        hdu = fits.PrimaryHDU(map, header=hdr)
        hdulist = fits.HDUList([hdu])

        # Write DataMap to FITS file, overwriting it if the file already exists
        hdulist.writeto(fits_fname, clobber=True)
        print "FITS File '%s' saved." % fits_fname

    def add_quiver_layer(self):
        pass

    def _h5_serialize(self, h5group, **kwargs):
        """
        Serialize the DataMap object into a HDF5 file.

        Parameters
        ----------
        h5group: ``h5py.Group``
            Main group to serialize the DataMap into
        float32: ``bool``
            save map data with dtype=float32 instead of float64 ? default False
        save_mask: ``bool``
            save map mask alongside the map data ? default True. if True, saves the camera map_mask, used to set
            transparency when there is no intersection between the map and the simulation domain.
        """
        float32 = kwargs.get("float32", False)
        save_mask = kwargs.get("save_mask", True)

        # DataMap main attributes
        h5group.attrs['DATAMAP_VERSION'] = DataMap.__version

        # Save camera object
        self._camera.save_HDF5(h5group)

        if float32:
            # use appropriate type to save disk space :
            smap = self._vmap.astype("float32")
        else:
            smap = self._vmap

        # Save map data
        nx, ny = self._camera.get_map_size()
        map_size = N.array([nx, ny])
        map_range = N.array([N.min(smap), N.max(smap)])

        map_group = h5group.create_group("map")

        # Save map name, if any
        if self.name is not None:
            map_group.attrs['name'] = unicode(self.name)

        map_group.create_dataset("data", data=smap)

        if save_mask:
            if float32:
                smask = self._vmap.astype("float32")
            else:
                smask = self._map_mask
            map_group.create_dataset("mask", data=smask)

        map_group.create_dataset("size", data=map_size)
        map_group.create_dataset("value_range", data=map_range)

        unit_group = map_group.create_group("unit")
        self._unit.save_HDF5(unit_group)

    @classmethod
    def _h5_deserialize(cls, h5group):
        """
        Read a DataMap object from a HDF5 file.

        Parameters
        ----------
        h5group: ``h5py.Group``
            Main group to deserialize the DataMap from.

        Returns
        -------
        dm: :class:`~pymses.analysis.datamap.DataMap`
            new Datamap instance
        """
        # Check DataMap object version written in HDF5 file
        if "DATAMAP_VERSION" not in h5group.attrs:
            raise AttributeError("Cannot find a valid DataMap object in '%s'." % h5group.name)

        vers = h5group.attrs['DATAMAP_VERSION']
        if vers == DataMap.__version:  # Current DataMap version
            cam = Camera.from_HDF5(h5group)
            map_group = h5group['map']

            map = map_group['data'][...]

            if 'name' in map_group.attrs:
                name = map_group.attrs['name']
            else:
                name = None

            # mask = map_group['mask'][...]
            # map_size = map_group['size'][...]
            # map_range = map_group['value_range'][...]

            value_unit = Unit.from_HDF5(map_group['unit'])

        else:
            raise ValueError("Unknown DataMap version (%d)" % int(vers))

        m = cls(map, cam, value_unit, name=name)
        return m

    @classmethod
    def load_legacy_HDF5_datamap(cls, h5fname):
        """
        Read a DataMap object from a legacy (PyMSES v<=4.0.0) HDF5 file.

        Parameters
        ----------
        h5fname: ``string``
            HDF5 file name.

        Returns
        -------
        dmap: :class:`~pymses.analysis.datamap.DataMap`
            DataMap instance
        """
        import h5py
        try:
            import tables as T
        except ImportError:
            raise ImportError("PyTables module is required to handle legacy HDF5 files in PyMSES !")

        ######################################################################################
        #      Old Pymses (v<=4.0.0) HDF5 camera/map files written with PyTables check       #
        ######################################################################################
        f0 = h5py.File(h5fname, 'r')
        if 'camera' not in f0 or'CAMERA_VERSION' in f0['camera'].attrs:
            f0.close()
            raise AttributeError("'%s' file is not a valid (legacy) Camera File" % h5fname)
        f0.close()
        ######################################################################################
        try:
            h5f = T.File(h5fname, 'r')
            h5g = h5f.get_node("/")

            # Old datamap version (PyTables syntax)
            map_name = h5g.name.read()
            cam = Camera._load_legacy_cam(h5g)
            map_group = h5g.map
            map = map_group.map.read()
            # Mandatory map value unit
            value_unit = Unit._load_legacy_HDF5_unit(map_group.unit)
            # Mandatory size unit unit => save it into Camera instance
            cam.size_unit = Unit._load_legacy_HDF5_unit(map_group.length_unit)

            vrange_path = map_group._g_join("value_range")
            if h5f.__contains__(vrange_path):
                map_range = map_group.value_range.read()
            else:
                map_range = None

            msize_path = map_group._g_join("size")
            if h5f.__contains__(msize_path):
                map_size = map_group.size.read()
            else:
                map_size = None

            dmap = cls(map, cam, value_unit, name=map_name)
        except Exception as exc:
            raise IOError("HDF5 I/O error : %s" % exc)
        finally:
            h5f.close()

        return dmap

    @classmethod
    def value_range_from_multiple_HDF5(cls, h5fname_iter, fraction=None):
        """
        Get the common value range from a DataMap HDF5 file iterator

        Parameters
        ----------
        h5fname_iter: DataMap HDF5 filename iterator

        Returns
        -------
        vrange: ``tuple`` of ``float``
            (vmin, vmax) global value range
        """
        all_vmin = None
        all_vmax = None
        for h5fname in h5fname_iter:
            d = DataMap.from_HDF5(h5fname)

            if fraction is not None:
                vmin, vmax = d.value_range(fraction=fraction)
            else:
                vmin, vmax = d.value_range()

            if all_vmin is None:
                all_vmin = vmin
            elif vmin < all_vmin:
                all_vmin = vmin
            if all_vmax is None:
                all_vmax = vmax
            elif vmax > all_vmax:
                all_vmax = vmax

        return all_vmin, all_vmax

__all__ = ["DataMap"]
