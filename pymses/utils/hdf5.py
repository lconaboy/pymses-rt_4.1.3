# -*- coding: utf-8 -*-
#   This file is part of PyMSES.
#
#   PyMSES is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyMSES is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyMSES.  If not, see <http://www.gnu.org/licenses/>.
"""
:mod:`pymses.utils.hdf5`: HDF5 serializable interface
-----------------------------------------------------
"""

import numpy
from .filename import FileUtil

try:
    import h5py
    _h5py_available = True
except ImportError as imperr:
    _h5py_available = False


class HDF5Serializable(object):
    def _h5_serialize(self, h5group, **kwargs):
        raise NotImplementedError()

    def save_HDF5(self, h5fg, **kwargs):
        """
        Returns an serialisable object from a HDF5 file.

        Parameters
        ----------
        h5fg: ``str`` or ``h5py.File`` or ``h5py.Group``
            HDF5 (h5py) file/group or filename.

        Raises
        ------
        imperr: ``ImportError``
            raises an ImportError if the 'h5py' module is not found.
        atterr: ``AttributeError``
            raises an AttributeError if any attribute is not valid.
        ioerr: ``IOError``
            raises an IOError if an error occured while writing into the HDF5 file.
        """
        if isinstance(h5fg, h5py.Group):
            group = h5fg
            close_when_done = False
            f = None
        else:
            if "where" not in kwargs:
                where = "/"
            else:
                where = kwargs.pop("where")
            f, group, close_when_done = self.__open_h5file(h5fg, where, create=True)

        try:
            self._h5_serialize(group, **kwargs)
        except Exception as exc:
            raise IOError("HDF5 I/O error : %s" % exc)
        finally:
            if close_when_done and f is not None:
                f.close()

    @classmethod
    def __open_h5file(cls, h5f, where, create=False):
        """
        Returns a HDF5 tables.File object

        Parameters
        ----------
        h5f: ``str`` or ``h5py.File``
            HDF5 (h5py) File object or filename
        where: ``string``
            location of the object in the HDF5 tree
        create: ``bool``
            Open file in writing mode ?

        Returns
        -------
        f: ``h5py.File``
            HDF5 file object.
        g: ``h5py.Group``
            HDF5 group object.

        Raises
        ------
        imperr: ``ImportError``
            raises an ImportError if the 'h5py'  module is not found.
        atterr: ``AttributeError``
            raises an AttributeError if any attribute is not valid.
        """
        if not _h5py_available:
            raise ImportError("h5py module is required to handle HDF5 files in PyMSES !")

        # Check the 'where' kwarg is a valid string.
        if not isinstance(where, basestring):
            raise AttributeError("'where' attribute must be a string. Got '%s'." % type(where))

        if isinstance(h5f, basestring):
            if create:
                f = h5py.File(FileUtil.new_filepath(h5f, append_extension=FileUtil.HDF5_FILE), 'w')
            else:  # Read an existing HDF5 file
                f = h5py.File(FileUtil.valid_filepath(h5f, append_extension=FileUtil.HDF5_FILE), 'r')

            close_when_done = True
        elif isinstance(h5f, h5py.File):
            f = h5f
            close_when_done = False
        else:
            raise AttributeError("'h5f' must be a HDF5 file path or a valid h5py.File object. Got '%s'" % type(h5f))

        # Get base Group
        group = f[where]

        return f, group, close_when_done

    @classmethod
    def _h5_deserialize(cls, h5group):
        raise NotImplementedError()

    @classmethod
    def from_HDF5(cls, h5fg, where="/"):
        """
        Returns an serialisable object from a HDF5 file.

        Parameters
        ----------
        h5fg: ``str`` or ``h5py.File`` or ``h5py.Group``
            HDF5 (h5py) file/group or filename.
        where: ``string``
            location of the object in the HDF5 tree. Default is tree root ("/").

        Raises
        ------
        imperr: ``ImportError``
            raises an ImportError if the 'h5py' module is not found.
        atterr: ``AttributeError``
            raises an AttributeError if any attribute is not valid.
        ioerr: ``IOError``
            raises an IOError if an error occured while reading the HDF5 file.
        """
        if isinstance(h5fg, h5py.Group):
            group = h5fg
            close_when_done = False
            f = None
        else:
            f, group, close_when_done = cls.__open_h5file(h5fg, where)

        try:
            o = cls._h5_deserialize(group)
        except Exception as exc:
            raise IOError("HDF5 I/O error : %s" % exc)
        finally:
            if close_when_done and f is not None:
                f.close()

        return o

__all__ = ["HDF5Serializable"]
