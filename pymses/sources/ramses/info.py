# -*- coding: utf-8 -*-
#   This file is part of PyMSES.
#
#   PyMSES is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyMSES is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyMSES.  If not, see <http://www.gnu.org/licenses/>.
"""
info.py -- RAMSES .info reading routine
"""

import re
import os
import ast
import numpy
import pymses.utils.constants as C
from ._read_ramses import read_hydro_header


def _is_number(str):
    '''
    Dirty check to support scientific notation
    '''
    try:
        float(str)
        return True
    except ValueError:
        return False


def read_ramses_rt_info_file(info_filename):
    """ Reads a RAMSES-RT .info file, returning a dictionary of parameter values
    """

    info_dict = {}
    info_fileobj = open(info_filename, 'r')

    re_split = re.compile("\s*=\s*")
    re_emptyline = re.compile("^\s*$")
    re_header_props = re.compile("\s*Photon\s*[\sa-zA-Z]+[-]+")

    par_dict = {}
    is_param = True
    group_num = None

    # TODO: some code cleanup down there
    for line in info_fileobj.readlines():
        if re_emptyline.match(line):
            continue
        if re_header_props.match(line):
            is_param = False
            continue
        params = re_split.split(line)
        if is_param:
            par_name, par_val = params
            par_dict[par_name] = par_val
        else:
            # Group props
            par_name = line.strip().split(" ")[0]
            if par_name.strip().startswith('---'):
                group_num = line.strip()[3:]
                par_dict[group_num] = []
                continue
            if par_dict.has_key(group_num):
                lines = line.strip().split(" ")
                par_dict[group_num].append({lines[0]:
                                    [float(lines[i]) for i in range(1, len(lines)) if _is_number(lines[i])]})
            else:
                lines = line.strip().split(" ")
                par_dict[par_name] = [float(lines[i]) for i in range(1, len(lines)) if _is_number(lines[i])]

    info_dict["nRTvar"] = int(par_dict["nRTvar"])
    info_dict["nIons"] = int(par_dict["nIons"])
    info_dict["nGroups"] = int(par_dict["nGroups"])
    info_dict["iIons"] = int(par_dict["iIons"])
    info_dict["X_fraction"] = float(par_dict["X_fraction"])
    info_dict["Y_fraction"] = float(par_dict["Y_fraction"])
    info_dict["unit_photon_number"] = float(par_dict["unit_np"])*C.cm**-3  # photon number density scale [# cm-3]
    info_dict["unit_photon_flux_density"] = float(par_dict["unit_pf"]) * (C.cm**-2 * C.s**-1)  # [# cm-2 s-1]
    info_dict["rt_c_frac"] = float(par_dict["rt_c_frac"])
    info_dict["n_star"] = float(par_dict["n_star"])*C.H_cc # H/cc
    info_dict["T2_star"] = float(par_dict["T2_star"])*C.K # K/mu
    info_dict["g_star"] = float(par_dict["g_star"])

    # Read group energies

    info_dict["photon_properties"] = {"groupL0":numpy.array(par_dict["groupL0"]) * C.eV, "groupL1":numpy.array(par_dict["groupL1"]) * C.eV}
    for i in range(info_dict["nGroups"]):
        # Unpack
        vals = par_dict["Group %i"%(i+1)]
        info_dict["group%i"%(i+1)] = dict((k,numpy.array(v, dtype=float)) for d in vals for (k,v) in d.items())

        info_dict["group%i"%(i+1)]["egy"] = info_dict["group%i"%(i+1)]["egy"]*C.eV
        info_dict["group%i"%(i+1)]["csn"] = info_dict["group%i"%(i+1)]["csn"]*C.cm**2
        info_dict["group%i"%(i+1)]["cse"] = info_dict["group%i"%(i+1)]["cse"]*C.cm**2

    info_fileobj.close()
    return info_dict


def read_ramses_info_file(info_filename):
    """ Reads a RAMSES .info file, returning a dictionary of parameter values
    """
    if not os.path.isfile(info_filename):
        raise AttributeError("Cannot find RAMSES output info file '%s'." % info_filename)

    re_kwline = re.compile(r"^\w+\s*=\s*.+$")
    re_split = re.compile(r"\s*=\s*")
    re_split2 = re.compile(r"\s*")
    re_emptyline = re.compile(r"^\s*$")
    re_header_boundkeys = re.compile(r"^\s*DOMAIN\s*ind_min\s*ind_max\s*$")

    bound_table = None
    par_dict = {}
    is_param = True

    with open(info_filename, 'r') as info_fileobj:
        # Info.txt file parsing
        for line in info_fileobj.readlines():
            if re_emptyline.match(line) is not None:
                continue
            if re_header_boundkeys.match(line) is not None:
                is_param = False
                continue
            if re_kwline.match(line) is not None and not is_param:
                is_param = True
            if is_param:
                # Parameter parsing
                params = re_split.split(line)
                par_name = params[0].strip()
                par_val = params[1].strip().strip("\n")
                if par_name == "ordering type":
                    par_dict["ordering"] = par_val
                    if par_val != "hilbert":
                        continue
                    else:
                        bound_table = []
                else:
                    par_dict[par_name] = par_val
            else:
                # Boundary keys parsing
                params = re_split2.split(line)
                bound_table.append([float(params[2]), float(params[3].strip().strip("\n"))])

    info_dict = {}
    for key, value in par_dict.items():
        if key in ["ncpu", "ndim", "levelmin", "levelmax", "ngridmax", "nstep_coarse"]:
            info_dict[key] = int(value)
        elif key in ["boxlen", "time", "aexp", "H0", "omega_m", "omega_l", "omega_k", "omega_b"]:
            info_dict[key] = float(value)
        elif key == "ordering type":
            info_dict["ordering"] = value
        elif key == "unit_l":
            unit_l = float(value)
            info_dict["unit_length"] = unit_l * C.cm
        elif key == "unit_d":
            unit_d = float(value)
            info_dict["unit_density"] = unit_d * C.g / C.cm ** 3
        elif key == "unit_t":
            unit_t = float(value)
            info_dict["unit_time"] = unit_t * C.s
        else:  # Custom (user's RAMSES patch) parameters
            try:
                info_dict[key] = ast.literal_eval(value)
            except ValueError:  # Cannot evaluate value type, maybe it really was a string ?
                info_dict[key] = value

    # -------------------------------------- Unit post-processing --------------------------------------------- #
    info_dict["unit_velocity"] = info_dict["unit_length"] / info_dict["unit_time"]
    info_dict["unit_pressure"] = info_dict["unit_density"] * info_dict["unit_velocity"] ** 2
    info_dict["unit_temperature"] = info_dict["unit_velocity"] ** 2 * C.mH / C.kB
    info_dict["unit_mag"] = numpy.sqrt(4 * numpy.pi * unit_d * (unit_l / unit_t) ** 2) * C.Gauss
    info_dict["unit_gravpot"] = info_dict["unit_velocity"]**2
    # To use only for collisionless particles (needs to ben done BEFORE 'unit_length' renormalisation by boxlen).
    # Bug introduced by revision 897 (May, 16, 2013)
    # see issues #6 (https://bitbucket.org/dchapon/pymses/issues/6/)
    #            #10 (https://bitbucket.org/dchapon/pymses/issues/10/).
    info_dict["unit_mass"] = info_dict["unit_density"] * info_dict["unit_length"] ** 3
    # Boxlen renormalisation
    info_dict["unit_length"] = info_dict["unit_length"] * info_dict["boxlen"]
    info_dict["unit_gravpot"] = info_dict["unit_gravpot"] * info_dict["boxlen"]

    # --------------------------------------------------------------------------------------------------------- #

    if info_dict["ordering"] == "hilbert":
        keys = numpy.zeros(info_dict["ncpu"] + 1)
        bound_table = numpy.asarray(bound_table)
        keys[0] = bound_table[0, 0]
        keys[1:] = bound_table[:, 1]
        info_dict["dom_decomp_Hilbert_keys"] = keys
    else:
        info_dict["dom_decomp"] = None

    return info_dict


def read_ramses_hydro_header(hydro_filename, swap=False):
    hydro_hdr = read_hydro_header(hydro_filename, int(swap))
    return {"nvar_file": hydro_hdr["nvar_file"],
            "gamma": hydro_hdr["gamma"]}


__all__ = ["read_ramses_info_file", "read_ramses_hydro_header"]
