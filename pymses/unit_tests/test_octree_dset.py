# -*- coding: utf-8 -*-
#   This file is part of PyMSES.
#
#   PyMSES is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   PyMSES is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with PyMSES.  If not, see <http://www.gnu.org/licenses/>.
"""
test_octree_dset.py -- test module for Hilbert space-filling curve
"""

import pymses
import numpy
from pymses.analysis.splatting import SplatterProcessor
from pymses.analysis.transfer_functions import ColorLinesTransferFunction
from pymses.analysis import Camera, ScalarOperator, slicing, raytracing
import os
from pymses.utils import misc, constants as C


PWD = os.path.abspath(os.path.dirname(__file__))

asserted_SliceMap_result = numpy.array([[-5.98442495, -5.98442495, -6.00830233],
                                        [-5.98442495, -5.98442495, -6.00830233],
                                        [-6.03202943, -6.03202943, -6.07758903]])

asserted_interpolated_SliceMap_result = numpy.array([[-5.99014809, -5.96351829, -5.96968206],
                                                     [-6.01119872, -5.99727729, -5.99045202],
                                                     [-6.02887729, -6.01696159, -6.01550138]])

asserted_rt_map = numpy.array([[-4.61547079, -4.50387064, -4.50387064],
                               [-5.17418872, -4.88956574, -4.88956574],
                               [-5.17418872, -4.88956574, -4.88956574]])

asserted_rotated_rt_map = numpy.array([[4.43912944e-06, 9.40160105e-06, 6.78405933e-06],
                                       [5.16985554e-06, 3.88652536e-05, 4.32777455e-05],
                                       [2.06883743e-06, 7.71202140e-06, 8.58699623e-06]])

asserted_lvlmax_map = numpy.array([[3, 3, 3],
                                   [3, 3, 3],
                                   [3, 3, 3]])


class EmptyOutput(object):
    def __init__(self):
        self.info = {}
        self.info["levelmin"] = 3
        self.info["dom_decomp"] = None
        self.output_repos = None
        self.iout = None


def test_loading_OctreeDataset():
    amr_source = pymses.sources.ramses.octree.OctreeDataset.from_hdf5(os.path.join(PWD, "test_amr_dset.h5"))
    assert (amr_source.get_read_levelmax() == 3)
    assert (amr_source.data_list == [0])
    assert (amr_source.fields["rho"].shape[0] == 73)
    assert (amr_source.fields["rho"][5][5] - 1.33670771473e-07 < 1e-6)


def test_SliceMap():
    amr_source = pymses.sources.ramses.octree.OctreeDataset.from_hdf5(os.path.join(PWD, "test_amr_dset.h5"))
    rho_op = ScalarOperator(lambda dset: dset["rho"], C.H_cc)
    cam = Camera(center=[0.5, 0.5, 0.5], line_of_sight_axis='z', up_vector='y', region_size=[.3, .3], distance=0.,
                 far_cut_depth=0., map_max_size=3, log_sensitive=True)
    datamap = slicing.SliceMap(amr_source, cam, rho_op, z=0.3)
    assert (abs(numpy.log10(datamap.map) - asserted_SliceMap_result) < 1e-6).all()


def test_SliceMap_interpolation():
    amr_source = pymses.sources.ramses.octree.OctreeDataset.from_hdf5(os.path.join(PWD, "test_amr_dset.h5"))
    rho_op = ScalarOperator(lambda dset: dset["rho"], C.H_cc)
    cam = Camera(center=[0.5, 0.5, 0.5], line_of_sight_axis='z', region_size=[.3, .3], distance=0., far_cut_depth=0.,
                 up_vector='y', map_max_size=3, log_sensitive=True)
    datamap = slicing.SliceMap(amr_source, cam, rho_op, z=0.3, interpolation=True)
    assert (abs(numpy.log10(datamap.map) - asserted_interpolated_SliceMap_result) < 1e-6).all()


def test_loading_camera():
    cam = Camera.from_HDF5(os.path.join(PWD, "camera_octree_dset.h5"))
    center = [0.49414021, 0.48413301, 0.49849942]
    line_of_sight = [0.59924788, 0.32748786, 0.73051603]
    assert (abs(cam.los_axis - line_of_sight) < 1e-5).all()
    assert (abs(cam.center - center) < 1e-5).all()


def test_RayTracer():
    octree_dset = pymses.sources.ramses.octree.OctreeDataset.from_hdf5(os.path.join(PWD, "test_amr_dset.h5"))
    rho_op = ScalarOperator(lambda dset: dset["rho"], C.H_cc)
    cam = Camera(center=[0.5, 0.5, 0.5], line_of_sight_axis='z', up_vector='y', region_size=[.3, .3], distance=0.3,
                 far_cut_depth=0.3, map_max_size=3, log_sensitive=False)

    ro = EmptyOutput()
    rt = raytracing.RayTracer(octree_dset, ro.info, rho_op)
    # rt.disable_multiprocessing()
    dmap = rt.process(cam, surf_qty=False)
    print dmap.map, asserted_rt_map
    assert (abs(numpy.log10(dmap.map) - asserted_rt_map) < 1e-6).all()


def test_RayTracer_rotated():
    octree_dset = pymses.sources.ramses.octree.CameraOctreeDataset.from_hdf5(os.path.join(PWD, "test_amr_dset.h5"))
    rho_op = ScalarOperator(lambda dset: dset["rho"], C.H_cc)
    # camera center small shift is still with OctreeRayTracer to avoid grid limit pb
    cam = Camera(center=[0.501, 0.501, 0.501], line_of_sight_axis=[0.401, 0.601, -0.701], up_vector='y',
                 region_size=[.3, .3], distance=0.3, far_cut_depth=0.3, map_max_size=3, log_sensitive=False)

    ro = EmptyOutput()
    rt = raytracing.RayTracer(octree_dset, ro.info, rho_op)
    dmap = rt.process(cam, surf_qty=False)
    assert (abs(dmap.map - asserted_rotated_rt_map) < 1e-3).all()


def test_Splatting():
    octree_dset = pymses.sources.ramses.octree.OctreeDataset.from_hdf5(os.path.join(PWD, "test_amr_dset.h5"))
    rho_op = ScalarOperator(lambda dset: dset["rho"], C.H_cc)
    cam = Camera(center=[0.5, 0.5, 0.5], line_of_sight_axis='z', up_vector='y', region_size=[.3, .3], distance=0.3,
                 far_cut_depth=0.3, map_max_size=3, log_sensitive=True)

    class EmptyOutput(object):
        def __init__(self):
            self.info = {}
            self.info["levelmin"] = 3
            self.info["levelmax"] = 3
            self.info["dom_decomp"] = None
            self.output_repos = None
            self.iout = None

    ro = EmptyOutput()

    mp = SplatterProcessor(octree_dset, ro.info, rho_op)
    # mp.disable_multiprocessing()
    map = mp.process(cam, pre_flatten=False, surf_qty=False)

    # map = ImgPlot.apply_log_scale(map)
    print numpy.log10(map.map)
    asserted_map = numpy.array([[-0.22320506, -0.0246566,  -0.11069895],
                                [-0.2586495,  -0.0441908,  -0.11421175],
                                [-0.67037297, -0.41799685, -0.45085401]])
    print asserted_map
    assert (abs(numpy.log10(map.map) - asserted_map) < 1e-6).all()


# def test_OctreeRayTracer_RGB():
# 	octree_dset = pymses.sources.ramses.octree.CameraOctreeDataset.from_hdf5(os.path.join(PWD, "test_amr_dset.h5"))
# 	rho_op = ScalarOperator(lambda dset: dset["rho"], C.H_cc)
# 	cam = Camera(center=[0.5, 0.5, 0.5], line_of_sight_axis='z', up_vector='y', region_size=[.3, .3], distance=0.3,
# 				 far_cut_depth=0.3, map_max_size=3, log_sensitive=True)
#
# 	cltf = ColorLinesTransferFunction((-5.0, 2.0))
# 	cltf.add_line(-2.0, 0.1)
# 	cltf.add_line(.0, 0.1)
# 	cltf.add_line(2., 0.1)
# 	cam.set_color_transfer_function(cltf)
#
# 	rt = raytracing.OctreeRayTracer(octree_dset)
# 	map = rt.process(rho_op, cam, return_image=False)
#
# 	print map[0]
# 	asserted_map = numpy.array([[0.12494199, 0., 0.57994005],
# 								[0.12494199, 0., 0.57994005],
# 								[0.12446702, 0., 0.57773542]])
# 	print asserted_map
# 	assert (abs(map[0] - asserted_map) < 1e-6).all()
